
local fname = minetest.get_worldpath() .. DIR_DELIM .. chat_dat.settings.file_handle.file_name
if chat_dat.settings.file_handle.include_port == true then
    fname = fname .. tostring(chat_dat.settings.file_handle.port)
end
fname = fname .. ".dat"

chat_dat._post = function (msg)
    local fh = io.open(fname, "a")
    if fh == nil then
        minetest.log("warn", "[chat_dat] Post failed, unable to open file '" .. fname .. "' in append mode!")
        return
    end
    fh:write(msg .. "\n")
    fh:close()
end
