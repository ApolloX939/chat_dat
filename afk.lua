
chat_dat.cache.afk_list = {}

minetest.register_on_joinplayer(function (player, last_on)
    local name = player:get_player_name()
    chat_dat.cache.afk_list[name] = false
end)

minetest.register_on_newplayer(function (player)
    local name = player:get_player_name()
    chat_dat.cache.afk_list[name] = false
end)

minetest.register_on_leaveplayer(function (name, timed_out)
    -- Don't notify for players who have left (possibly because they got kicked)
    chat_dat.cache.afk_list[name] = nil
end)

chat_dat._loop = function ()
    local hits = {}
    -- Process players that have just become afk
    for name, afk_dur in pairs(chat_dat.cache.afk.get_all_longer_than(chat_dat.settings.afk.trigger)) do
        hits[name] = true
        if chat_dat.cache.afk_list[name] ~= true and chat_dat.cache.afk_list[name] ~= nil then
            chat_dat._post("* " .. name .. " is now AFK")
            chat_dat.cache.afk_list[name] = true
        end
    end
    -- Process players that have returned from afk
    for name, status in pairs(chat_dat.cache.afk_list) do
        if hits[name] == nil and status == true then
            chat_dat._post("* " .. name .. " is no longer AFK")
            chat_dat.cache.afk_list[name] = false
        end
    end
    minetest.after(1, chat_dat._loop)
end
minetest.after(1, chat_dat._loop)
