
if chat_dat.settings.events.on_joinplayer == true then
    minetest.register_on_joinplayer(function (player, last_on)
        local name = player:get_player_name()
        chat_dat._post("* " .. name .. " has joined")
    end)
end

if chat_dat.settings.events.on_leaveplayer == true then
    minetest.register_on_leaveplayer(function (player, timed_out)
        local name = player:get_player_name()
        if timed_out then
            chat_dat._post("* " .. name .. " has left (timed out)")
        else
            chat_dat._post("* " .. name .. " has left")
        end
    end)
end

if chat_dat.settings.events.on_newplayer == true then
    minetest.register_on_newplayer(function (player)
        local name = player:get_player_name()
        chat_dat._post("* " .. name .. " has joined (first time)")
    end)
end

if chat_dat.settings.events.on_dieplayer == true then
    minetest.register_on_dieplayer(function (player, reason)
        local name = player:get_player_name()
        -- Try to determine the cause, if able (else fallback to generic)
        if reason.type == "fall" then
            chat_dat._post("* " .. name .. " has died (fell from a great height)")
        elseif reason.type == "drown" then
            chat_dat._post("* " .. name .. " has died (drowned)")
        elseif reason.type == "node_damage" and string.match(reason.node, "lava") ~= nil then
            chat_dat._post("* " .. name .. " has died (swam in lava)")
        elseif reason.type == "node_damage" and string.match(reason.node, "fire") ~= nil then
            chat_dat._post("* " .. name .. " has died (burned)")
        else
            chat_dat._post("* " .. name .. " has died")
        end
    end)
end

if chat_dat.settings.events.on_chat_message == true then
    minetest.register_on_chat_message(function (name, msg)
        chat_dat._post("<" .. name .. "> " .. msg)
    end)
    -- Also handle the '/me' chat command
    minetest.register_on_chatcommand(function (name, cmd, params)
        if cmd ~= "me" then return end
        chat_dat._post("* " .. name .. " " .. params)
    end)
end
