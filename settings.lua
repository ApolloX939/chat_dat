
-- File Handle
local fname = tostring(minetest.settings:get("chat_dat.file_name"))
if fname ~= nil then
    chat_dat.settings.file_handle.file_name = fname
end

local use_port = minetest.settings:get_bool("chat_dat.include_port", true)
if use_port == false then
    chat_dat.settings.file_handle.include_port = false
end

local port_num = tonumber(minetest.settings:get("chat_dat.port"))
if port_num ~= 30000 and port_num ~= "fail" and port_num ~= nil then
    chat_dat.settings.file_handle.port = port_num
end

-- Events

local on_join = minetest.settings:get_bool("chat_dat.on_joinplayer", true)
if on_join == false then
    chat_dat.settings.events.on_joinplayer = false
end

local on_part = minetest.settings:get_bool("chat_dat.on_leaveplayer", true)
if on_leave == false then
    chat_dat.settings.events.on_leaveplayer = false
end

local on_chat = minetest.settings:get_bool("chat_dat.on_chat_message", true)
if on_chat == false then
    chat_dat.settings.events.on_chat_message = false
end

local on_newjoin = minetest.settings:get_bool("chat_dat.on_newplayer", false)
if on_newjoin == true then
    chat_dat.settings.events.on_newplayer = true
end

local on_die = minetest.settings:get_bool("chat_dat.on_dieplayer", false)
if on_die == true then
    chat_dat.settings.events.on_dieplayer = true
end

local on_afk = minetest.settings:get_bool("chat_dat.on_afk", false)
if on_afk == true then
    chat_dat.settings.events.on_afk = true
end

-- Afk

if on_afk == true then
    local trigger = tonumber(minetest.settings:get("chat_dat.afk_trigger"))
    if trigger ~= "fail" and trigger ~= 120.0 then
        chat_dat.settings.afk.trigger = trigger
    end
end
