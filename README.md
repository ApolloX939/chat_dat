# Chat Data: Collector

This mod is used to collect chat events and store them into a file.

What events are permitted to be logged are in the settingtypes.txt file.

A list of those events:

* on_joinplayer \*
* on_leaveplayer \*
* on_chat_message \*
* on_newplayer
* on_dieplayer
* afk_indicator.get_all_longer_than

\* Are included by default configuration.

> The [afk_indicator](https://content.minetest.net/packages/Emojiminetest/afk_indicator/) mod is required if the `afk_indicator.get_all_longer_than` (which will notify as soon as a player is afk for more than defined, default of 120 seconds)

