
chat_dat = {
    version = "v1.0-dev",
    cache = {},
    settings = { -- Defaults
        file_handle = {
            file_name = "chat_dat",
            include_port = true,
            port = 30000
        },
        events = {
            on_joinplayer = true,
            on_leaveplayer = true,
            on_chat_message = true,
            on_newplayer = false,
            on_dieplayer = false,
            on_afk = false
        },
        afk = {
            trigger = 120.0
        }
    }
}

local runfile = function (dir, file)
    local mod = minetest.get_modpath("chat_dat")
    if file then
        dofile(mod .. DIR_DELIM .. dir .. DIR_DELIM .. file .. ".lua")
    else
        dofile(mod .. DIR_DELIM .. dir .. ".lua")
    end
end

runfile("settings")

if chat_dat.settings.events.on_afk then
    chat_dat.cache.afk = rawget(_G, "afk_indicator") or nil
    if chat_dat.cache.afk == nil then
        error("[chat_dat] Player AFK Event requires 'afk_indicator', mod not found!")
    end
end

runfile("logger")
runfile("events")

if chat_dat.settings.events.on_afk then
    runfile("afk")
end
